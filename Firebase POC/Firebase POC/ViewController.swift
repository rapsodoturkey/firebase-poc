//
//  ViewController.swift
//  Firebase POC
//
//  Created by Mehmet Okay on 17.01.2022.
//

import UIKit
import Firebase
import FirebaseDatabase

class ViewController: UIViewController {
    var firstFirebase: Bool = true
    @IBOutlet weak var switchButton: UISwitch!
    @IBOutlet weak var sendButton: UIButton!
    var num = 1
    var ref: DatabaseReference!
    var ref2: DatabaseReference!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        ref = Database.database().reference()
        ref2 = Database.database().reference()
    }
    @IBAction func sendPressed(_ sender: UIButton) {
        
        let secondaryOptions = FirebaseOptions(googleAppID: "1:42249207559:ios:e4f2cc00d316c5127e0fa2",
                                               gcmSenderID: "42249207559")
        secondaryOptions.apiKey = "AIzaSyD2JCgP_v4BfuDaAK37n2bFngZVazaJdmg"
        secondaryOptions.projectID = "fir-poc2-77303"
        
        // Configure an alternative FIRApp.
        FirebaseApp.configure(name: "secondary", options: secondaryOptions)

        // Retrieve a previous created named app.
        guard let secondary = FirebaseApp.app(name: "secondary")
          else { assert(false, "Could not retrieve secondary app") }


        // Retrieve a Real Time Database client configured against a specific app.
        let secondaryDb = Database.database(app: secondary)
        ref2 = Database.database(app: secondary).reference()
        ref2.child("deneme").setValue("denedim\(num)")
        ref.child("deneme").setValue("denedimM")
        num += 1
    }
    
    @IBAction func switchChanged(_ sender: UISwitch) {
        if firstFirebase{
            firstFirebase = false
            if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
                myDelegate.changePlistFile(fileName: "GoogleService2-Info")
            }
        }
        else{
            firstFirebase = true
            if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
                myDelegate.changePlistFile(fileName: "GoogleService1-Info")
                
                
            }
            
        }
        
    }
}

